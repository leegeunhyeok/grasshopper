"""
@author: Leegeunhyeok
@date: 2019.03.14
"""

import subprocess
import configparser

def exec_command(*args):
    try:
        subprocess.run(*args, check=True, shell=True)
        return True
    except Exception as e:
        print('ERROR:', e)
        return False

if __name__ == '__main__':
    if not exec_command(['git', '--version']):
        exit()

    # TODO: Check git repo
